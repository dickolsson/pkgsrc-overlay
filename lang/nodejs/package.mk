# $NetBSD$

# Installation should always be done in production mode.
# Also handle slight inconsistencies between node packages.
INSTALL_ENV+=	NODE_ENV=production NODE_CONFIG_ENV=production

NPM_BUILD_COMMANDS?=	build

.include "../../mk/bsd.fast.prefs.mk"

do-build:
	# Install all dependencies. We are not using INSTALL_ENV here.
	# This stage is primarily for building dependencies.
	(cd ${WRKSRC} && ${SETENV} ${MAKE_ENV} \
	 ${PREFIX}/bin/npm install)

# Some packages require specific build commands to assemble asset etc.
# All commands need to run with INSTALL_ENV to use the right environment.
.for _COMMAND in ${NPM_BUILD_COMMANDS}
	(cd ${WRKSRC} && ${SETENV} ${MAKE_ENV} ${INSTALL_ENV} \
	 ${PREFIX}/bin/npm run ${_COMMAND})
.endfor

	# In order to properly install into DESTDIR, the package needs
	# to be installed as a tarball. So we pack it here.
	(cd ${WRKSRC} && ${SETENV} ${MAKE_ENV} ${INSTALL_ENV} \
	 ${PREFIX}/bin/npm pack)

do-install:
	# Install from the packaged tarball to DESTDIR.
	(cd ${WRKSRC} && ${SETENV} ${MAKE_ENV} ${INSTALL_ENV} \
	 ${PREFIX}/bin/npm install --global --prefix=${DESTDIR}${PREFIX} \
	 ${PKGNAME_NOREV}.tgz)

.include "../../lang/nodejs/nodeversion.mk"
.include "../../lang/npm/npmversion.mk"
