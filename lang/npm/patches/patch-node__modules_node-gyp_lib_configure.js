$NetBSD$

--- node_modules/node-gyp/lib/configure.js.orig	2021-09-23 20:20:00.000000000 +0000
+++ node_modules/node-gyp/lib/configure.js
@@ -14,21 +14,14 @@ if (win) {
 }
 
 function configure (gyp, argv, callback) {
-  var python
+  var python = '@PYTHONBIN@'
   var buildDir = path.resolve('build')
   var configNames = ['config.gypi', 'common.gypi']
   var configs = []
   var nodeDir
   var release = processRelease(argv, gyp, process.version, process.release)
 
-  findPython(gyp.opts.python, function (err, found) {
-    if (err) {
-      callback(err)
-    } else {
-      python = found
-      getNodeDir()
-    }
-  })
+  getNodeDir()
 
   function getNodeDir () {
     // 'python' should be set by now
