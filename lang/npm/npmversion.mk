# $NetBSD$

# This file determins which npm version is used as a dependency for
# a package.
#
# === User-settable variables ===
#
# NPM_VERSION_DEFAULT
#	The preferred npm version to use.
#
#	Possible values: 6 7
#	Default: 6
#
# === Infrastructure variables ===
#
# NPM_VERSION_REQD
#	npm version to use. This variable should not be set by
#	packages. Normally it is used by bulk build tools.
#
#	Possible values: ${NPM_VERSIONS_ACCEPTED}
#	Default: ${NPM_VERSION_DEFAULT}
#
# === Package-settable variables ===
#
# NPM_VERSIONS_ACCEPTED
#	The npm versions that the package can build against. Order
#	is significant; those listed earlier are preferred over those
#	listed later.
#
#	Possible values: 6 7
#	Default: 6 7
#
# NPM_VERSIONS_INCOMPATIBLE
#	The npm versions that the package *cannot* build against.
#
#	Possible values: 6 7
#	Default: <empty>
#
# Keywords: npm
#

.if !defined (NPM_NPMVERSION_MK)
NPM_NPMVERSION_MK=	# defined

# derive a npm version from the package name if possible
# optionally handled quoted package names
.if defined(PKGNAME_REQD) && !empty(PKGNAME_REQD:Mnpm[0-9]-*) || \
    defined(PKGNAME_REQD) && !empty(PKGNAME_REQD:M*-npm[0-9]-*)
NPM_VERSION_REQD?=	${PKGNAME_REQD:C/(^.*-|^)npm([0-9])-.*/\2/}
.elif defined(PKGNAME_OLD) && !empty(PKGNAME_OLD:Mnpm[0-9]-*) || \
    defined(PKGNAME_OLD) && !empty(PKGNAME_OLD:M*-npm[0-9]-*)
NPM_VERSION_REQD?=	${PKGNAME_OLD:C/(^.*-|^)npm([0-9])-.*/\2/}
.endif

.include "../../mk/bsd.prefs.mk"

BUILD_DEFS+=		NPM_VERSION_DEFAULT
BUILD_DEFS_EFFECTS+=	NPM_PACKAGE

NPM_VERSION_DEFAULT?=		6
NPM_VERSIONS_ACCEPTED?=		6 7
NPM_VERSIONS_INCOMPATIBLE?=	# empty

# Resolve NPM_VERSIONS_INCOMPATIBLE and generate the _OK vars.
.for v in ${NPM_VERSIONS_ACCEPTED}
.  if empty(NPM_VERSIONS_INCOMPATIBLE:M${v})
_NPM_VERSION_${v}_OK=		yes
_NPM_VERSIONS_ACCEPTED+=	${v}
.  endif
.endfor

# Pick a version
.if defined(NPM_VERSION_REQD)
.  if defined(_NPM_VERSION_${NPM_VERSION_REQD}_OK)
_NPM_VERSION=	${NPM_VERSION_REQD}
.  endif
.else
.  if defined(_NPM_VERSION_${NPM_VERSION_DEFAULT}_OK)
_NPM_VERSION?=	${NPM_VERSION_DEFAULT}
.  endif
.  for v in ${_NPM_VERSIONS_ACCEPTED}
.    if defined(_NPM_VERSION_${v}_OK)
_NPM_VERSION?=	${v}
.    endif
.  endfor
.endif

# In case nothing matched
_NPM_VERSION?=		none

.if ${_NPM_VERSION} == "6"
.include "../../lang/npm6/buildlink3.mk"
.elif ${_NPM_VERSION} == "7"
.include "../../lang/npm/buildlink3.mk"
.else
PKG_FAIL_REASON+=	"No valid npm version found"
.endif

# Variable assignment for multi-npm packages
MULTI+=	NPM_VERSION_REQD=${_NPM_VERSION}

.endif  # NPM_NPMVERSION_MK
