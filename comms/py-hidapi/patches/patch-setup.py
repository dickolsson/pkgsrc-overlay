$NetBSD$

--- setup.py.orig	2020-11-26 10:15:14.000000000 +0000
+++ setup.py
@@ -18,7 +18,17 @@ if '--with-system-hidapi' in sys.argv:
     system_hidapi = 1
     hidapi_include = '/usr/include/hidapi'
 
-if sys.platform.startswith('linux'):
+if "CFLAGS" in os.environ and "LDFLAGS" in os.environ:
+    modules = [
+        Extension(
+            "hid",
+            sources=src,
+            extra_compile_args=os.environ["CFLAGS"].split(),
+            extra_link_args=os.environ["LDFLAGS"].split(),
+        )
+    ]
+
+elif sys.platform.startswith('linux'):
     modules = []
     if '--without-libusb' in sys.argv:
         sys.argv.remove('--without-libusb')
@@ -51,7 +61,7 @@ if sys.platform.startswith('linux'):
         )
     )
 
-if sys.platform.startswith('darwin'):
+elif sys.platform.startswith('darwin'):
     macos_sdk_path = subprocess.check_output(['xcrun', '--show-sdk-path']).decode().strip()
     os.environ['CFLAGS'] = '-isysroot "%s" -framework IOKit -framework CoreFoundation -framework AppKit' % macos_sdk_path
     os.environ['LDFLAGS'] = ''
@@ -67,7 +77,7 @@ if sys.platform.startswith('darwin'):
         )
     ]
 
-if sys.platform.startswith('win') or sys.platform.startswith('cygwin'):
+elif sys.platform.startswith('win') or sys.platform.startswith('cygwin'):
     libs = ['setupapi']
     if system_hidapi == True:
         libs.append('hidapi')
@@ -81,7 +91,7 @@ if sys.platform.startswith('win') or sys
         )
     ]
 
-if 'bsd' in sys.platform:
+elif 'bsd' in sys.platform:
     if 'freebsd' in sys.platform:
         libs = ['usb', 'hidapi']
         include_dirs_bsd = ['/usr/local/include/hidapi']
